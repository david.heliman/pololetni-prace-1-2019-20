/*

Napište program, který vytiskne list kalendáře - tabulku s jedním měsícem.

Program bude ovládán argumenty: bez argumentů vypíše aktuální měsíc,
s jedním argumentem vypíše měsíc současného roku a se 2 argumenty pak
kalendář s daným měsícem a rokem.

> java Kalendar
// Kalendar pro soucasny mesic
> java Kalendar 10
// Kalendar pro rijen soucasneho roku
> java Kalendar 2 2005
// Kalendar unora 2005

Kalendář by měl vypadat zhruba takhle:

+----------------------+
| cervenec 2014        |
+----------------------+
| Po Ut St Ct Pa So Ne |
|     1  2  3  4  5  6 |
|  7  8  9 10 11 12 13 |
| 14 15 16 17 18 19 20 |
| 21 22 23 24 25 26 27 |
| 28 29 30 31          |
+----------------------+

Jak lze v Javě zjistit současné datum najděte na internetu.

Rozšíření (1b): Program bude vypisovat 3 měsíce: vybraný měsíc doplněný
kalendářem pro předchozí a následující měsíc.

.----------------------.  .----------------------.  .----------------------.
|       listopad       |  |       prosinec       |  |        leden         |
|----------------------|  |----------------------|  |----------------------|
| Po Ut St Ct Pa So Ne |  | Po Ut St Ct Pa So Ne |  | Po Ut St Ct Pa So Ne |
|                 1  2 |  |  1  2  3  4  5  6  7 |  |           1  2  3  4 |
|  3  4  5  6  7  8  9 |  |  8  9 10 11 12 13 14 |  |  5  6  7  8  9 10 11 |
| 10 11 12 13 14 15 16 |  | 15 16 17 18 19 20 21 |  | 12 13 14 15 16 17 18 |
| 17 18 19 20 21 22 23 |  | 22 23 24 25 26 27 28 |  | 19 20 21 22 23 24 25 |
| 24 25 26 27 28 29 30 |  | 29 30 31             |  | 26 27 28 29 30 31    |
|                      |  |                      |  |                      |
`----------------------'  `----------------------'  `----------------------'

*/

public class Kalendar {
    public static void main(String args[]) {
    }
}