/*

Napište program pro vytvoření náhledů z více obrázků.
Program je variací na existující programy z [lekce 6](../pg09/index.html).
Funkce navíc jsou 2: program poskládá vytvořené náhledy z více obrázků vedle sebe (do mřížky) a jeho
chování půjde ovlivňovat pomocí tzv. přepínačů.

Pokud uživatel nespecifikuje jinak, bude mít matice velikost 4x6 obrázků, každý náhled 300x150 pixelů,
výsledný obrázek bude pojmenován nahledy-001.jpg, nahledy-002.jpg atd.
Zmenšovaní obrázků zachová poměr stran, barva na pozadí bude černá.
V takovém případě bude ke spuštění programu stačit uvést jako parametry seznam vstupních obrázků:

java -cp .:awh.jar Nahledy obr1.jpg obr2.jpg obr3.jpg obr4.jpg ...

Pokud bude chtít uživatel přepsat některou z výchozích hodnot, může k tomu využít tzv. přepínače:

 * --vystup=matice-%04d.jpg: výstup bude uložen do souborů matice-0001.jpg, matice-0002.jpg atd.
 * --matice=2x3: každý výstupní soubor bude obsahovat 6 náhledů (2 sloupce)
 * --nahled=60x40: náhledy budou mít velikost 60x40 pixelů (60 na šířku)
 * --nahled=60x40!: s vykřičníkem: náhledy budou deformované bez ohledu na poměr stran
 * --pozadi=ff0000: barva pozadí v hexadecimálním zápisu (využijte metodu String.charAt)

*/

public class Nahledy {
    public static void main(String args[]) {
    }
}
